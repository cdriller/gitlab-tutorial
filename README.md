# Tutorial - Zusammenarbeit in GitLab

In diesem Tutorial sollen einige Möglichkeiten gezeigt werden wie in GitLab zusammengearbeitet werden kann.

Punkte die gezeigt werden:

- Unterschied der Ablagemöglichkeiten: Snippets und GitLab Projekte - Was ergibt in welchen Situationen mehr Sinn?
- Was sind Merge Requests?
- Was ist ein Fork?
- Wie halten wir unseren GitLab Server aufgeräumt, sodass wir effizient zusammenarbeiten können?

Habt ihr andere Fragen, die wir behandeln wollen?


## 1. Unterschied GitLab Snippets und GitLab Projekten

Ihr habt folgendes Skript entwickelt und lokal abliegen.

```powershell
Get-Process -Name *microsoft*,*windows* | Format-Table -Property ID, ProcessName
```

Ihr wollt es für andere Kollegen auf GitLab zur Verfügung stellen. Aber **wie** und **wohin** genau in GitLab?

Erstmal zu dem **_Wie_**:

Fürs Erste machen wir das einfach mit Copy and Paste, da wir uns in diesem Tutorial nicht auf das Setup für `git pull` und `git push` konzentrieren wollen.

Desweiteren ist bei dem _Wie_ wichtig zu erwähnen, dass ihr eine Beschreibung haben solltet. Folgendes beschreibt dein Skript ziemlich gut:

```
Tabelarische Auflistung aller laufenden Prozesse, die in ihrem Namen 'windows' oder 'microsoft' haben.
```

Ok das sollte es zu dem _Wie_ gewesen sein, kümmern wir uns jetzt also um das **_Wohin_**:

Die zwei generellen Optionen, die dir zur Verfügung stehen sind **Snippets und Projekte**. Hier eine kleine Gegenüberstellung von den beiden:

|Snippets|Projekte|
|-|-|
| Sehr einfach zu erstellen | Etwas schwieriger zu erstellen |
| geringer Funktionsumfang | Großer Funktionsumfang |
| wenige Möglichkeiten zusammenzuarbeiten | viele Mögöichkeiten für die Zusammenarbeit |
| wenig Möglichkeiten zu dokumentieren | viele Möglichkeiten zu dokumentieren und zu erklären |

Es können Snippets [mit](https://gitlab.com/cdriller/my-first-project/-/snippets/2219144) und [ohne](https://gitlab.com/-/snippets/2219143) Projektbezug erstellt werden.
Projekte können dem entsprechenden User zugewiesen werden (wie dieses Projekt dem user cdriller zugewiesen wurde) oder einer Gruppe ([Ein Beispiel](https://gitlab.com/irs-gitlab/group-project)), sodass direkt mehrere User Zugriff auf das Projekt haben.

### Soll ich lieber Snippets oder Projekte nutzen?

Snippets solltest du quasi als erweiterete Zwischenablage nutzen. Für Code, den du dir einfach merken willst, aber nicht unbedingt dafür, das andere User diesen Code nutzen sollen. Wenn du den Code wirklich teilen willst solltest du das mit einem Projekt tun.

### Wie kann ich dafür sorgen, dass andere mein Skript auch sehen können? (Berechtigungen und Visibility)

Es gibt zwei Möglichkeiten einzustellen, dass andere dein Skript sehen können.

1. Visibility
2. Berechtigungen

Wie der Begriff schon vermuten lässt, beschreibt die Visibilty nur die Sichtbarkeit (also die Leserechte) des Projekts. Die drei Stufen der Visibilty sind:

- private (nur User mit entsprechenden Berechtigungen haben Zugriff)
- internal (nur angemeldete User haben Zugriff)
- public (alle, die den Server erreichen, können auch auf das Projekt Zugreifen)

> Zugriff auf das Projekt bedeutet bei er Visibility auch Zugriff auf den Code

Und dann gibt es noch Berechtigungen die über Rollen an User vergeben werden können. Die Berechtigungen in GitLab sind folgende (in Klammern eine kurze Beschreibung was sie können) 

- Guest (Kann auf das Projekt zugreifen, aber hat keine Leserechte für den Code)
- Reporter (Guest + kann Code sehen)
- Developer (Schreibrechte außer für protected Branches)
- Maintainer (Schreibrechte auch für protected Branches)
- Owner (Darf alles)


## 2. Was sind Merge Requests? Was ist ein Fork?

> Grundsätzlich sind Merge Requests und Forks beides Features, die es ermöglichen, dass jeder weiter an einem Softwareprojekt entiwckeln kann.

Gucken wir uns zwei Szenarien an um Forks und Merge Requests zu verstehen

### Szenario 1 (Merge Request): Developer will Änderungen in 'main' Branch einspielen

Ein Developer hat Schreibrechte auf das Repository, jedoch nicht auf den protected Branches (der 'main' Branch ist in der Regel protected). Somit kann der Developer einen neuen Branch anlegen und sein neues Feature auf diesem Branch entwickeln. Wenn er fertig ist, fragt er mit einem `Merge Request` , ob ein Maintainer oder Owner das neue Feature in den 'main' Branch mergen kann.

```mermaid
graph TB

  subgraph "Main Project"
  Commit5 -- Merge Request --> Commit4
  Commit5[Commit 5 feature Branch] --> Commit3
  Commit4[Commit 4 main Branch] --> Commit2
  Commit3[Commit 3] --> Commit2
  Commit2[Commit 2] --> Commit1[Commit 1]
end
```

### Szenario 2 (Fork und Merge Request): Du willst an einem public Projekt mitarbeiten

Da du bei einem public Projekt in der Regel überhaupt keine Schreibberechtigungen hast, gibt es Forks. Bei einem Fork wird eine Kopie des Projekts in deinem eigenen Account erstellt. Jedoch mit einem Verweis auf das ursprüngliche Projekt, sodass du auch von deinem Projekt aus einen Merge Request in das Ursprungsprojekt stellen kannst.

```mermaid
graph TB

  Commit1 -- Fork --> ForkedCommit1
  ForkedCommit3 -- Merge Request --> Commit2
  subgraph "Forked Project"
  ForkedCommit3[Commit 3 main Branch] --> ForkedCommit2[Commit 2]
  ForkedCommit2[Commit 2 ] --> ForkedCommit1[Commit 1]
  end

  subgraph "Main Project"
  Commit2[Commit 2 main Branch] --> Commit1[Commit 1]
end
```


## 3. Wie halten wir unseren GitLab Server aufgeräumt, sodass wir effizient arbeiten können?

- Es gibt Gruppen für die einzelnen Abteilungen. Alles was unter der jeweiligen Abteilung angelegt wird fällt unter die Verantwortung von dieser Abteilung. Es könnnen auch einzelne Berechtigungen auf bestimmte Projekte vergeben werden, sodass abteilungsübergreifend zusammen an Projekten gearbeitet werden kann.

- Es gibt eine Gruppe IRS in der allgemeine Sachen abgelegt werden. Unter anderem das Projekt 'Konzept Softwareentwicklung'. Dieses Projekt ist eine Ansammlung von Markdown Files in denen wir definieren, wie wir in GitLab arbeiten wollen, **macht gerne Merge Requests für das Projekt 'Konzept Softwareentwicklung', wenn ihr Verbesserungsvorschläge habt, wie wir GitLab nutzen können!** 



